This repository has been created to illustrate the deployment of a dynamic web page on OpenShift.

### Instructions ###

Create your OpenShift account with a free plan

1. Create one new application with a generic cartridge (e.g., php)
1. Indicate the address of this repository as **Source Code**, and take a note of the **Public URL** 
1. Wait for the application to be ready
1. See the result with a browser at your **Public URL**
 
If you want to modify the code, clone the repository (the URL is in the **Source Code** box on the right), edit, commit and push.